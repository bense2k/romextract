#!/bin/sh

# Extraction script for:
# Metroid Prime (USA).iso
# Metroid Prime (USA) (Rev 2).iso

# Outputs:
# - Metroid (USA).nes

# Requires: wit, decryptmetroid

# Thanks to FIX94 releasing decryptmetroid.c under https://gist.github.com/FIX94/7593640c5cee6c37e3b23e7fcf8fe5b7

romextract()
{
	dependency_wit            || return 1
	dependency_decryptmetroid || return 1

	echo "Extracting file from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
		--files=+/files/NESemu.rel

	if [ "$GAMETITLE" = "Metroid Prime (USA)" ]; then
		VERSION="U0"
	elif [ "$GAMETITLE" = "Metroid Prime (USA) (Rev 1)" ]; then ## Unsupported
		VERSION="U1"
		else
		VERSION="U2"
	fi

	echo "Extracting ROM ..."
	"$DECRYPTMETROID_PATH" "$VERSION" "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GM8E/files/NESemu.rel" \
		"$SCRIPTID/Metroid (USA).nes"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
