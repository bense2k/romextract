#!/bin/sh

# Extraction script for:
# 8-bit Adventure Anthology: Volume I (resources.assets)

# Outputs:
# - Deja Vu (8-bit Adventure Anthology) (Unverified).nes
# - Uninvited (8-bit Adventure Anthology) (Unverified).nes
# - Shadowgate (8-bit Adventure Anthology) (Unverified).nes

romextract()
{
	
	if [ -f "$(dirname "$FILE")/game-x86_64_Data/resources.assets" ]; then
		FILE="$(dirname "$FILE")/game-x86_64_Data/resources.assets"
	elif [ -f "$(dirname "$FILE")/game_Data/resources.assets" ]; then
		FILE="$(dirname "$FILE")/game_Data/resources.assets"
	else
		echo "Could not find resources.assets"
		return 1
	fi

	echo "Extracting ROMs from resources.assets ..."
	offset=$(strings -a -t d "$FILE" | awk "/Deja_Vu$/ {print \$1}")
	tail -c +$((offset+13)) "$FILE" | head -c +393232 \
		> "$SCRIPTID/Deja Vu (8-bit Adventure Anthology) (Unverified).nes" 2>/dev/null
	offset=$(strings -a -t d "$FILE" | awk "/Uninvited$/ {print \$1}")
	tail -c +$((offset+17)) "$FILE" | head -c +262160 \
		> "$SCRIPTID/Uninvited (8-bit Adventure Anthology) (Unverified).nes" 2>/dev/null
	offset=$(strings -a -t d "$FILE" | awk "/Shadowgate$/ {print \$1}")
	tail -c +$((offset+17)) "$FILE" | head -c +262160 \
		> "$SCRIPTID/Shadowgate (8-bit Adventure Anthology) (Unverified).nes" 2>/dev/null

	echo "Script $SCRIPTID.sh done"
}
