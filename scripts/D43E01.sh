#!/bin/sh

# Extraction script for:
# Legend of Zelda, The - Ocarina of Time & Master Quest (USA).iso

# Outputs:
# - Legend of Zelda, The - Ocarina of Time (USA) (Collectors Edition).z64
# - Legend of Zelda, The - Ocarina of Time - Master Quest (USA) (GameCube).z64

# Requires: wit

romextract()
{
	dependency_wit || return 1

	echo "Extrating file from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
		--files=+/files/zlj_f.tgc

	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +476234209 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-D43E/files/zlj_f.tgc" \
		| head -c +33554432 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time - Master Quest (USA) (GameCube).z64"
	tail -c +510421465 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-D43E/files/zlj_f.tgc" \
		| head -c +33554432 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time (USA) (Collector's Edition).z64"
	# TODO: Don't know the actual length of the movie in pal_zelda_w_snd.thp,
	#       for now just cut from the beginning of the file to the THP header
	#tail -c +2114813 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-D43E/files/zlj_f.tgc" \
	#	| head -c +465809792 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time Credits (USA) (Collector's Edition).thp"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
